from scenarios.simulator_scenario import SimulatorScenario
from simulator.antenna import Antenna
from simulator.config.simulator_config import SimulatorConfig
from simulator.config.simulator_user import SimulatorUser


class SampleSimulatorScenario(SimulatorScenario):

    def name(self):
        return "sample"

    def load_config(self)->SimulatorConfig:
        config = SimulatorConfig(
            power=30,
            stepAmount=10000,
            ndlrb=10,
            frequency=3e9,
            antenna_amount=64,
            users=[],
            lambda_=0.1,
            cell_size=[8, 8],
            noise=3.16228e-20,
            delta=30,
            antenna = Antenna(
                coordinates=[0, 0, 20],
                tilt=0)
        )
        config.users.append(SimulatorUser(
            user_id=1, x=100,y=100,z=0,error_level=10,antenna_amt=1,noise=10
            , start_time=150, stop_time=5200, size_from=20000, size_to=50000
        ))
        config.users.append(SimulatorUser(
            user_id=2, x=100, y=-100, z=0, error_level=10, antenna_amt=1,noise=10
            , start_time=50, stop_time=5000, size_from=50000, size_to=60000
        ))
        return config
