from abc import ABC, abstractmethod

from simulator.config.simulator_config import SimulatorConfig


class SimulatorScenario(ABC):
    @abstractmethod
    def name(self):
        pass

    @abstractmethod
    def load_config(self)->SimulatorConfig:
        pass