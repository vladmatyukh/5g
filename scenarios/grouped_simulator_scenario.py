import random

from scenarios.simulator_scenario import SimulatorScenario
from simulator.antenna import Antenna
from simulator.config.simulator_config import SimulatorConfig
from simulator.config.simulator_user import SimulatorUser


class GroupedSimulatorScenario(SimulatorScenario):

    def name(self):
        return "grouped"

    def __init__(self, user_amount=30):
        self.user_amount = user_amount

    def load_config(self) -> SimulatorConfig:

        config = SimulatorConfig(
            power=40,
            stepAmount=10000,
            ndlrb=10,
            frequency=3e9,
            antenna_amount=64,
            users=[],
            lambda_=0.1,
            cell_size=[8, 8],
            noise=3.16228e-20,
            delta=30,
            antenna=Antenna(
                coordinates=[0,0,20],
                tilt=0,
            )
        )
        for i in range(self.user_amount):
            user = SimulatorUser(
                user_id = i + 1,
                x = (random.random() - 0.5) * 60,
                y = 100 + (random.random() - 0.5) * 20,
                z = 1.7 + (random.random() - 0.5)/2,
                error_level = 10,
                antenna_amt = 1,
                noise = config.noise,
                start_time = 150 + random.randint(100, 2000),
                stop_time = 5000 + random.randint(100, 2000),
                size_from = 20000,
                size_to = 50000)
            config.users.append(user)
        return config