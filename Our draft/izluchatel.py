from INFO import *
from interfais import Execute
import numpy as np
import math
import cmath
import json
from numpy import linalg as LA
m_lambda = 0.1 #длина волны
m_x_size = 8
m_y_size = 8
antenna_x = []
antenna_y = []
antenna_z = []
x = 0;
for i in range(m_x_size):
    y = i * m_lambda / 2 - (m_x_size - 1) / 4 * m_lambda;
    for j in range(m_y_size):
        z = j * m_lambda / 2 - (m_y_size - 1) / 4 * m_lambda;

        antenna_x.append(x)
        antenna_y.append(y)
        antenna_z.append(z)
class Izluchat(Execute):
    def __init__(self, user):
        with open('koordinat', 'r') as f:
            data = json.loads(f.read())
            for i in data['users']['user']:
                if i['ID'] == user:
                    self.x = i['x']
                    self.y = i['y']
                    self.z = i['z']

        H = []
        for i in range(m_x_size):
            for j in range(m_y_size):
                r = math.sqrt(
                    pow(self.x - antenna_x[j + m_x_size * i], 2) +
                    pow(self.y - antenna_y[j + m_x_size * i], 2) +
                    pow(self.z - 30 - antenna_y[j + m_x_size * i], 2)
                );

                cur_phase = 2 * math.pi / m_lambda * r
                H.append(cmath.exp(complex(0, cur_phase)))
        self.phase1 = H
    #def takew(self):
        self.p = 100
        self.w = (H / LA.norm(H)) * np.sqrt(self.p)
