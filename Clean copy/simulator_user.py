class SimulatorUser:
    def __init__(self, user_id: int, x: float, y: float, z: float, data_type='FULL_BUFFER', data_amt: int = 1024*8,
                 antenna_amount: int = 1):
        self.user_id = user_id
        self.data_amt = data_amt
        self.data_type = data_type
        self.antenna_amount = antenna_amount
        self.z = z
        self.y = y
        self.x = x
