import json
from collections import namedtuple
from simulator_user import SimulatorUser

data = '{"name": "John Smith", "hometown": {"name": "New York", "id": 123}}'

# Parse JSON into an object with attributes corresponding to dict keys.
q = json.loads(data, object_hook=lambda d: namedtuple('X', d.keys())(*d.values()))
print(q.name, q.hometown.name, q.hometown.id)
simulatoruser = SimulatorUser(q.id, q.id.x, q.id.y, q.id.z, q.id.data_type, q.id.data_amt, q.id.antenna_amount)
