from simulator.executable import Executable


class Simulator:
    def __init__(self, stepAmt):
        self.stepAmt = stepAmt
        self.executables = []

    def append(self, executable: Executable):
        self.executables.append(executable)

    def run(self):
        for time in range(self.stepAmt):
            for executable in self.executables:
                executable.execute(time)

