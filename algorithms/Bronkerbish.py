#!/usr/bin/env python
# coding: utf-8

# In[43]:


import copy
import random as rd
import numpy as np


# In[44]:
gr = []

#new functions are created for this script
#including vertex degree, adjacency matrix and remove vertex
class graph:
    def __init__(self):
        self.graph={}
        self.visited={}        

    def append(self,vertexid,edge,weight):
        if vertexid not in self.graph.keys():          
            self.graph[vertexid]={}
            self.visited[vertexid]=0
        self.graph[vertexid][edge]=weight

    def reveal(self):
        return self.graph
    
    def vertex(self):
        return list(self.graph.keys())

    def edge(self,vertexid):
        return list(self.graph[vertexid].keys())
    
    def weight(self,vertexid,edge):
        
        return (self.graph[vertexid][edge])
    
    def size(self):
        return len(self.graph)
    
    def visit(self,vertexid):
        self.visited[vertexid]=1
    
    def go(self,vertexid):
        return self.visited[vertexid]
    
    def route(self):
        return self.visited
    
    def degree(self,vertexid):
        return len(self.graph[vertexid])
    
    def mat(self):        
        self.matrix = []
        print(self.graph)
        for i in self.graph:
            self.matrix.append([0 for k in range(len(self.graph))])
            for j in self.graph[i].keys():        
                self.matrix[i][j] = 1
        return self.matrix
    
    def remove(self,node):
        for i in self.graph[node].keys():
            self.graph[i].pop(node)
        self.graph.pop(node)


# In[45]:


#to get maximal clique
#we need an undirected graph
#in another word, vertices with edge connections 
#are mutually connected to each other
users = 7
df = graph()
#df.append(1,2,1)
#df.append(1,3,1)
#df.append(2,1,1)
#df.append(2,4,1)
#df.append(2,6,1)
#df.append(3,1,1)
#df.append(3,4,1)
#df.append(3,5,1)
#df.append(4,2,1)
#df.append(4,3,1)
#df.append(4,5,1)
#df.append(5,3,1)
#df.append(5,4,1)
#df.append(5,7,1)
#df.append(6,2,1)
#df.append(6,7,1)
#df.append(7,5,1)
#df.append(7,6,1)
#df.append(7,8,1)
#df.append(8,7,1)
h = np.array([0.5, 0.85, 0.29, 0.87, 0.4, 0.38, 0.64])
for i in range(users):
    for j in range(users):
        if h[i] * h[j] < 0.3 and i != j:
            df.append(i, j, 1)


# In[46]:


df.reveal()


# In[47]:


df.mat()


# In[66]:


#using brute force to solve maximal clique problem
#as maximal clique problem is np complete
#back tracking algorithm has very high time complexity
#the idea is to iterate all subsets and combinations
#and find out which is a maximal clique

#clique_min defines the minimum size of maximal clique
#in practical case, we dont want to include a two edge clique
def backtrack(graph, clique_min=2):
    
    #each vertex in the graph acts as a pivot
    #we form a new tree to check all the subsets that contain pivot
    queue=graph.vertex()
    
    #output stores all the maximal cliques
    output=[]
    
    #visited keeps track of all the pivot vertices we checked
    visited=[]

    for i in queue:
        
        #check all the vertices adjacent to the pivot i
        adjacency=graph.edge(i)

        for j in adjacency:
            
            #each edge that connects two vertices can form a clique
            clique=[i,j]
            
            #check all the vertices adjacent to both pivot i and current node j
            intersection=[node for node in graph.edge(j) if node in adjacency]
            
            #to remove duplicates and reduce iterations
            #we check the reverse order as well
            if f'{i}-{j}' in visited or f'{j}-{i}' in visited:
                continue

            visited.append(f'{i}-{j}')
            
            #the current node becomes k
            #both i and j are already in the clique
            #now we have to find a maximal clique
            for k in intersection:
                
                stop=False
                
                #if node k is adjacent to all the nodes in the existing clique
                for l in clique:
                    if k not in graph.edge(l):
                        stop=True
                        break

                if stop:
                    continue
                else:
                    clique.append(k)
            
            #minimum clique size is 2 by default
            #basically all edges are included
            #as this is an undirected graph
            #we can set it to 3 to be more useful
            #use sorted to avoid duplicates in the output
            if len(clique)>=clique_min and sorted(clique) not in output:
                output.append(sorted(clique))
                gr.append(sorted(clique))
                
    return output


# In[67]:


backtrack(df)
smcl = np.zeros(len(gr))
for time in range(30):
    weight1 = []
    for k in range(len(gr)):
        s = 0
        p = np.identity(len(gr[k]))
        for i in range(len(gr[k])):
            for j in range(len(gr[k])):
                if p[i][j] == 0:
                    s += df.graph[gr[k][i]][gr[k][j]]
                    p[i][j] = 1
                    p[j][i] = 1
        weight1.append(s)
    M = 0
    maxgraph = 0
    for k in range(len(gr)):
        if weight1[k] >= M:
            M = weight1[k]
            maxgraph = gr[k]
    smcl[gr.index(maxgraph)] += 1
    p1 = np.identity(len(maxgraph))
    for i in range(len(maxgraph)):
        for j in range(len(maxgraph)):
            if p1[i][j] == 0:
                df.graph[maxgraph[i]][maxgraph[j]] = df.graph[maxgraph[i]][maxgraph[j]] * 0.5
                p1[i][j] = 1
                p1[j][i] = 1
    print(weight1, ' Веса каждой клики')
    print(maxgraph, ' Выбранная клика на данной итерации')
    print(M, ' Вес выбранной клики')
print(smcl)
smpt = np.zeros(users)
for i in range(len(smcl)):
    for j in range(len(gr[i])):
        smpt[gr[i][j]] += smcl[[i]]
print(smpt)
print(gr)
print(sum(smpt))


# In[68]:





# In[13]:


def bron_kerbosch_pivot(df,R=set(),P=set(),X=set()):

    if not P and not X:
        yield R
    
    #the crucial part of pivoting is here
    #we choose a pivot vertex u from the union of pending and processed vertices
    #we delay the neighbors of pivot vertex from being added to the clique 
    #of course they will be added in the future recursive calls
    #we do that, we can make fewer iterations in recursion
    #if you count the recursive calls
    #you will find out pivot version reduce 2 recursive calls
    try:
        u=list(P.union(X)).pop()
        N=P.difference(df.edge(u))
    
    #sometimes our choice sucks
    #the neighbors of pivot u are equivalent to priority queue
    #in that case we just roll back to the function without pivoting
    except IndexError:
        N=P
    
    for v in N:
        
        yield from bron_kerbosch_pivot(df,
                                       R=R.union([v]),
                                       P=P.intersection(df.edge(v)),
                                       X=X.intersection(df.edge(v)))
        P.remove(v)
        X.add(v)


# In[14]:


#print(list(bron_kerbosch_pivot(df,P=set(df.vertex()))))


# In[ ]:




