from pulp import *
import numpy as np
from datetime import datetime
start_time = datetime.now()
p = [1, 1, 1, 1, 1, 1, 1]
s = [0, 0, 0, 0, 0, 0, 0]
t = 0
users = 7
for t in range(30):
    prob = LpProblem("Рюкзак", LpMaximize)
    h = np.array([0.5, 0.85, 0.29, 0.87, 0.4, 0.38, 0.64])
    B = np.zeros((users, users + 1))
    for i in range(users):
        for j in range(users):
            if h[i] * h[j] > 0.3 and i != j:
                B[i][j] = 1
            if i == j:
                B[i][j] = 0
    #A = np.array([[0, 1, 1, 0, 0, 0, 1, 1.0, 0], [1, 0, 1, 1, 0, 0, 0, 1.0, 1], [1, 1, 0, 1, 0, 1, 1, 1.0, 2],
     #             [0, 1, 1, 0, 1, 0, 1, 1.0, 3],
      #            [0, 0, 0, 1, 0, 1, 0, 1.0, 4], [0, 0, 1, 0, 1, 0, 0, 1.0, 5], [1, 0, 1, 1, 0, 0, 0, 1.0, 6]])
    #A = np.array([[0, 1, 0, 1, 0, 1], [1, 0, 0, 1, 1, 0], [0, 0, 0, 0, 0, 1], [1, 1, 0, 0, 0, 0], [0, 1, 0, 0, 0, 1], [1, 0, 1, 0, 1, 0]])
    var_names = range(users)
    x = LpVariable.dict('x', var_names, lowBound= 0, upBound= 1, cat='Integer')
    prob += lpSum(x[i] * p[i] for i in var_names)
    for i in range(len(var_names)):
        for j in range(len(var_names)):
            prob += x[i] * B[i][j] + x[j] * B[i][j] <= 1
    prob.solve(solver=CPLEX())
    print("Status:", LpStatus[prob.status])
    for i in range(len(var_names)):
        print(int(x[i].varValue), end=" ")
    print()
    print(datetime.now() - start_time)
    for i in range(len(var_names)):
        if x[i].varValue == 1:
            p[i] = 0.75 * p[i]
    for k in range(len(var_names)):
        s[k] += int(x[k].varValue)
    print(p)
print(s)
print(sum(s))