from simulator.antenna import Antenna


class SimulatorConfig:
    """
    https://sharetechnote.com/html/5G/5G_FrameStructure.html#SSB_CaseA_Tx_3Ghz
    """

    def __init__(self,
                 antenna: Antenna,
                 power, noise, stepAmount, ndlrb, frequency, antenna_amount, users, lambda_, cell_size, delta):
        """
        :param ndlrb: число ресурсных блоков
        :param frequency: средняя частот (для расчета длины волны и т.д.)
        :param cell_config: конфигурация соты
        :param users: пользователи
        """
        self.antenna = antenna
        self.noise = noise
        self.delta = delta
        self.cell_size = cell_size
        self.lambda_ = lambda_
        self.power = power
        self.stepAmount = stepAmount
        self.antenna_amount = antenna_amount
        self.frequency = frequency
        self.ndlrb = ndlrb
        self.users = users
