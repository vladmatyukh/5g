class SimulatorUser:
    def __init__(self, user_id, x, y, z, error_level, noise, antenna_amt=1,
                 strategy='FULL_BUFFER', start_time=50, stop_time=1000
                 , size_from=824, size_to=1224):
        self.noise = noise
        self.error_level = error_level
        self.antenna_amt = antenna_amt
        self.size_to = size_to
        self.size_from = size_from
        self.user_id = user_id
        self.stop_time = stop_time
        self.start_time = start_time
        self.z = z
        self.y = y
        self.x = x
        self.strategy = strategy
