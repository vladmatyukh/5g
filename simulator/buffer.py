class Buffer:
    def __init__(self):
        self.buffer_value = {}
        self.blocked_data = {}
        self.data_transferred = {}

    def append_user_data(self, time, user_id, data_amt):
        time_buffer_previous = self.buffer_value.get(time-1, None)
        time_buffer_current = self.buffer_value.get(time, {})
        if time_buffer_previous is None:
            time_buffer_current[user_id] = data_amt
        else:
            user_data = time_buffer_previous.get(user_id, 0)
            time_buffer_current[user_id] = data_amt + user_data
        self.buffer_value[time] = time_buffer_current

    def block_data(self, time, user_id, data_amt):
        time_buffer_current = self.buffer_value.get(time, {})
        user_data = time_buffer_current.get(user_id, 0)
        time_buffer_current[user_id] = max(user_data - data_amt, 0)

        block_data_current = self.blocked_data.get(time, {})
        blocked_amt = block_data_current.get(user_id, 0)
        block_data_current[user_id] = blocked_amt + min(data_amt, user_data)
        self.blocked_data[time] = block_data_current

    def success(self, time, user_id):
        block_data_current = self.blocked_data.get(time, {})
        user_data_transferred = self.data_transferred.get(time, {})
        user_data_transferred[user_id] = user_data_transferred.get(user_id, 0) + block_data_current.get(user_id, 0)
        self.data_transferred[time] = user_data_transferred

    def fail(self, time, user_id):
        block_data_current = self.blocked_data.get(time, {})
        blocked_amt = block_data_current.get(user_id, 0)
        time_buffer_current = self.buffer_value.get(time, {})
        user_data = time_buffer_current.get(user_id, 0)
        time_buffer_current[user_id] = user_data + blocked_amt
        self.buffer_value[time] = time_buffer_current
