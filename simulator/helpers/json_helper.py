import json


class JsonHelper:
    @staticmethod
    def save(file, obj):
        with open(file, "w") as fp:
            json.dump(obj, fp)
