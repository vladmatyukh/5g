import numpy as np
import math


class SinrHelper:
    def calculate_objective(self,
                       rb_channel: np.ndarray,
                       pw_dict,
                       precoding,
                       noise,
                       group, pfu, S):
        def sinr(user_id):
            usefull_signal = pw_dict[user_id] * np.dot(rb_channel, precoding.get_channel_matrix[user_id])
            interference = noise
            for users in range(len(S[group])):
                interference = interference + pw_dict[S[group][users]] * \
                               np.vdot(rb_channel, precoding.get_channel_matrix[S[group][users]])
            interference = interference - usefull_signal
            return usefull_signal/interference
        obj = sum(pfu[S[group][user]] *
                  math.log1p(sinr(S[group][user])) for user in range(len(S[group])))
        return obj
