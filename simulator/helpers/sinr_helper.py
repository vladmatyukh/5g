import numpy as np

from simulator.schedule import RbSchedule
from simulator.signals import RbSignal


class SinrHelper:
    @staticmethod
    def calculate_sinr(user_id,
                       users,
                       pw_dict,
                       rb_channel: np.ndarray,
                       rb_precoding,
                       power_denominator,
                       noise):
        transposed = rb_channel.transpose()
        signal = pw_dict[user_id]/power_denominator * np.abs(np.matmul(transposed, rb_precoding[user_id])[0][0])**2
        interference = 0
        for o_user_id in users:
            interference = interference + pw_dict[o_user_id]/power_denominator * \
                           np.abs(np.matmul(transposed, rb_precoding[o_user_id])[0][0])**2

        interference = interference - signal
        interference = interference + noise
        return signal/interference

    @staticmethod
    def calculate_su_sinr(channel: RbSignal, noise):
        res = 0
        for rb_channel in channel.rbs.values():
            sinr = np.matmul(rb_channel.transpose().conjugate(), rb_channel)**2/noise
            res = res + sinr
        return res/channel.rb_count

    @staticmethod
    def calculate_mu_sinr(user_id, users, pw_dict, rb_channel: np.ndarray, rb_precoding, cell_power, su_sinr):
        """

        :param user_id: идентификатор пользователя
        :param users: список пользователей
        :param pw_dict: распределение мощности
        :param rb_channel: вектор-канал
        :param rb_precoding: карта прекодингов
        :param cell_power: мощность на ресурсном блоке
        :param su_sinr: качество канала пользователя (измеренное)
        :return:
        """
        power_denominator = cell_power / pw_dict[user_id]
        interference = -1
        transposed = rb_channel.transpose()
        multiplier = np.abs(np.matmul(transposed, rb_precoding[user_id])[0][0])**2
        for o_user_id in users:
            interference = interference + pw_dict[o_user_id] / pw_dict[user_id] *\
                           np.abs(np.matmul(transposed, rb_precoding[o_user_id])[0][0])**2/multiplier
        return 1/(interference + power_denominator/su_sinr)