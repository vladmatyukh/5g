import random

import numpy as np

from simulator.config.simulator_user import SimulatorUser
from simulator.signals import RxSignal
from simulator.channel_data import ChannelData


class Receiver:
    def __init__(self, noise, user: SimulatorUser, buffer, channel_data: ChannelData):
        self.channel_data = channel_data
        self.noise = noise
        self.buffer = buffer
        self.user = user

    def receive(self, time, signal: RxSignal):
        avg_sinr_value = self.avg_sinr(signal)
        self.confirm(time, avg_sinr_value)
        self.update_channel(time, signal, avg_sinr_value)

    def avg_sinr(self, signal: RxSignal):
        """
        sinr = p_u * h * w_u / \sum_{u_1\in U} p_{u_1} * h * w_{u_1} + N
        :param signal:
        :return:
        """
        schedule = signal.tx_signal.schedule
        precoding_lst = signal.tx_signal.schedule.precoding_user_map
        sinr = 0
        cnt = 0
        user_id = self.user.user_id
        for rb_index, rb_schedule in schedule.rb_schedules.items():
            if rb_schedule.users.count(self.user.user_id) == 0:
                continue
            cnt = cnt + 1
            rb_channel = signal.channel_matrix.get_rb(rb_index)
            user_precoding = precoding_lst[rb_index][user_id]
            usefull_signal = rb_schedule.power[user_id] * np.abs(np.matmul(rb_channel.transpose(), user_precoding))**2
            interference = 0
            for o_user_id in rb_schedule.users:
                interference_precoding = precoding_lst[rb_index][o_user_id]
                interference = interference + \
                    rb_schedule.power[o_user_id] * np.abs(np.matmul(rb_channel.transpose(), interference_precoding))**2
            interference = interference - usefull_signal
            interference = interference + self.noise
            sinr = sinr + (usefull_signal/interference)

        if cnt > 0:
            return sinr/cnt
        else:
            return -1

    def update_channel(self, time, signal, avg_sinr):
        """
        Обновить данные канала и метрики согласно полученному сигналу
        :param avg_sinr:
        :param time:
        :param signal:
        :return:
        """
        self.channel_data.set_channel_matrix(self.user.user_id, signal.channel_matrix)
        if avg_sinr > 0:
            self.channel_data.sinr[self.user.user_id] = avg_sinr

    def confirm(self, time, sinr):
        """
        Проверка получения сигнала. Если успешно - убрать данные из буфера
        :param time:
        :param sinr:
        :return:
        """
        rand = random.random()
        error_level = rand * self.user.error_level
        if error_level < rand * sinr:
            self.buffer.success(time, self.user.user_id)
        else:
            self.buffer.fail(time, self.user.user_id)

