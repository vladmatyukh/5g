from simulator.buffer import Buffer
from simulator.channel_data import ChannelData
from simulator.helpers.sinrhelpernew import SinrHelper
from simulator.helpers.tb_size_helper import TbSizeHelper
from simulator.modules.module import Module
from simulator.schedule import Schedule, RbSchedule
from simulator.schedule_container import ScheduleContainer


import numpy as np


class Scheduler(Module):
    def execute(self, time):
        self.reassignment_pfu(time - 1)
        value = self.prepare_schedule()
        self.schedule_container.set_schedule(time, value)

    def measure_rates(self, time):
        rates = {}
        for user_id in self.channel_data.user_ids:
            user_sinr = self.channel_data.sinr[user_id]
            tb_size = TbSizeHelper.tb_size_from_sinr(user_sinr) * self.rb_count / self.rate_size
            rates[user_id] = tb_size

        for time_val in range(max(0, time - self.rate_size), time):
            transfer_data = self.buffer.data_transferred.get(time_val, {})
            for user_id in self.channel_data.user_ids:
                rates[user_id] = rates[user_id] + transfer_data.get(user_id, 0)/self.rate_size
        return rates

    def prepare_rb_schedule(self, rb_index)->RbSchedule:
        rb_schedule = RbSchedule(rb_index)

        def f(x, y):
            s = 0
            for k in range(y):
                s += x[k]
            return s

        def addition(x, y):
            if x + y == 0:
                return 0
            if x + y != 0:
                return 1

        A = self.do_graph.matrix_compilation()
        k = 0
        S = []
        dict1 = {}
        for i in range(A.shape[0]):
            dict1[i] = 0
        for i in range(A.shape[0]):
            S.append([])
            if dict1[i] == 0:
                S[k].append(i)
                dict1[i] = 1
            for j in range(A.shape[0]):
                if A[i][j] == 0 and dict1[j] == 0:
                    S[k].append(j)
                    dict1[j] = 1
                    for t in range(A.shape[0]):
                        A[i][t] = addition(A[i][t], A[j][t])
            k += 1
            if f(dict1, A.shape[0]) == A.shape[0]:
                break
        for j in range(len(S)):
            for i in range(len(S[j])):
                S[j][i] = self.do_graph.user_massive[S[j][i]]
        m = 0
        for group in range(len(S)):
            if SinrHelper.calculate_objective(1, 1, self.do_graph.channel_data, 1, group, self.do_graph.pfu, S) \
                    > SinrHelper.calculate_objective(1, 1, self.do_graph.channel_data, 1, m, self.do_graph.pfu, S):
                m = group

        for i in range(len(S[m])):
            rb_schedule.append_user(S[m][i])
        return rb_schedule

    def prepare_schedule(self):
        current_schedule = Schedule(self.rb_count)
        for rb_index in range(self.rb_count):
            a = self.prepare_rb_schedule(rb_index)
            current_schedule.append_rb_schedule(a)
        return current_schedule

    def reassignment_pfu(self, time):
        for user in self.do_graph.users:
            if self.buffer.data_transferred[time][user] > 0:
                self.do_graph.pfu[user] = time/((self.t[user] / self.do_graph.pfu[user])
                                                + self.buffer.data_transferred[time][user])
                self.t[user] = time

    def __init__(self,
                 rate_size,
                 rb_count,
                 buffer: Buffer,
                 schedule_container: ScheduleContainer,
                 channel_data: ChannelData, do_graph):
        self.do_graph = do_graph
        self.rb_count = rb_count
        self.rate_size = rate_size
        self.channel_data = channel_data
        self.schedule_container = schedule_container
        self.buffer = buffer
        self.t = np.ones(len(self.do_graph.users))
