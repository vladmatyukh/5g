from simulator.algorithms.schedule_algorithm import ScheduleAlgorithm
from simulator.buffer import Buffer
from simulator.channel_data import ChannelData
from simulator.helpers.sinr_helper import SinrHelper
from simulator.helpers.tb_size_helper import TbSizeHelper
from simulator.modules.module import Module
from simulator.precoding.mrt_precoding import Precoder
from simulator.schedule import Schedule, RbSchedule
from simulator.schedule_container import ScheduleContainer


class UserData:
    def __init__(self, user_id=None, user_metric=None):
        self.user_metric = user_metric
        self.user_id = user_id


class BasicScheduler(Module):
    def __init__(self,
                 schedule_algorithm: ScheduleAlgorithm,
                 schedule_container: ScheduleContainer, buffer: Buffer, channel_data: ChannelData, users,
                 rb_count, precoder: Precoder, noise, power,
                 delta=10):
        """

        :param schedule_container: контейнер с расписаниями
        :param buffer: информация
        :param channel_data: данные о каналах
        :param список идентификаторов пользователей
        :param delta: дельта за которое среднюю скорость считаем
        """
        self.schedule_algorithm = schedule_algorithm
        self.power = power
        self.noise = noise
        self.precoder = precoder
        self.rb_count = rb_count
        self.users = users
        self.delta = delta
        self.channel_data = channel_data
        self.buffer = buffer
        self.schedule_container = schedule_container

    def execute(self, time):
        transfer_data = self.buffer.buffer_value[time]
        metric = self.calculate_metric(time)
        schedule = self.build_schedule(transfer_data, metric)
        self.block_data(time, schedule)
        self.schedule_container.set_schedule(time, schedule)

    def calculate_metric(self, time):
        metric = {}
        for t in range(time - self.delta, time):
            data = self.buffer.data_transferred.get(t, None)
            if data is None:
                continue
            for user_id in self.users:
                metric[user_id] = metric.get(user_id, 1) + data.get(user_id, 0)/self.delta

        for user_id in self.users:
            metric[user_id] = metric.get(user_id, 1)
        return metric

    @staticmethod
    def clc_schedule_users(transfer_data, metric):
        sorted_users = {k: v for k, v in sorted(metric.items(), key=lambda item: item[1])}
        to_schedule_users = {}
        index = 0
        for user_id, user_metric in sorted_users.items():
            if transfer_data.get(user_id, 0) > 0:
                user_item = UserData(user_id, user_metric)
                to_schedule_users[index] = user_item
                index = index + 1
        return to_schedule_users

    def build_schedule(self, transfer_data, metric)->Schedule:
        sorted_users = self.clc_schedule_users(transfer_data, metric)
        return self.schedule_algorithm.build_schedule(sorted_users, self.rb_count, self.power, metric)
#        if sorted_users.__len__() == 0:
#            return None
#
#        schedule = Schedule(self.rb_count)
#
#        for rb_index in range(self.rb_count):
#            rb_schedule = RbSchedule(rb_index)
#            rb_schedule.append_user(sorted_users[rb_index % sorted_users.__len__()].user_id)
#            rb_schedule.specify_power(sorted_users[rb_index % sorted_users.__len__()].user_id, self.power)
#            rb_schedule.specify_signal(np.array([self.power]))
#            schedule.set_rb_schedule(rb_schedule)
#            schedule.power_signal.set_rb(rb_index, signal=np.array(self.power))
#        for user in sorted_users.values():
#            schedule.set_user_antenna_amount(user.user_id, 1)
#        self.precoder.build_precoding(precoding_matrix=schedule.precoding_matrix,
#                                      precoding_user_map=schedule.precoding_user_map,
#                                      schedule=schedule, channel_data=self.channel_data)
#        return schedule

    def block_data(self, time, schedule: Schedule):
        if schedule is None:
            return
        if schedule.rb_schedules is None:
            return
        if schedule.rb_schedules.__len__() == 0:
            return
        for rb_index, rb_schedule in schedule.rb_schedules.items():
            rb_precoding = schedule.precoding_user_map[rb_index]
            for user_id in rb_schedule.users:
                rb_channel = self.channel_data.get_channel_matrix(user_id=user_id).get_rb(rb_schedule.rb_index)
                sinr = SinrHelper.calculate_sinr(user_id=user_id, users=rb_schedule.users,
                                                 rb_channel=rb_channel,rb_precoding=rb_precoding,
                                                 noise=self.noise, pw_dict=rb_schedule.power, power_denominator=1)


                size = TbSizeHelper.tb_size_from_sinr(sinr)
#                print("sinr: {0}. size: {1}".format(sinr, size))
                self.buffer.block_data(time, user_id, size)
