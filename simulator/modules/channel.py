import cmath
import math

import numpy as np

from simulator.signals import TxSignal, RxSignal, RbSignalsHelper, RbSignal
from simulator.modules.receiver import Receiver
from simulator.modules.transmitter import Transmitter


class Channel:
    def __init__(self, transmitter: Transmitter, receiver: Receiver):
        self.transmitter = transmitter
        self.receiver = receiver
        self.antenna_coord = self.get_antenna_coord()

    def clc_channel_matrix(self, rb_count):
        phase_shift = self.clc_phase_shift(rb_count)
        propagation_loss = self.clc_propagation_loss()
        coord_ue = [self.receiver.user.x, self.receiver.user.y, self.receiver.user.z]
        pattern = self.transmitter.antenna.pattern(coord_ue)
        propagation_loss = propagation_loss * math.exp(pattern)
        channel_matrix = RbSignalsHelper.multiply_signal_number(phase_shift, propagation_loss)
        return channel_matrix

    def process(self, time, signal: TxSignal):
        channel_matrix = self.clc_channel_matrix(signal.signal.rb_count)
        rx_signal = RxSignal()
        rx_signal.tx_signal = signal
        rx_signal.channel_matrix = channel_matrix
        rx_signal.signal = RbSignalsHelper.multiply_component(channel_matrix, signal.signal)
        self.receiver.receive(time, rx_signal)

    def clc_phase_shift(self, rb_count)->RbSignal:
        lambda_ = self.transmitter.lambda_
        cell_size = self.transmitter.cell_size
        coord_cell = self.transmitter.antenna.coordinates
        coord_ue = [self.receiver.user.x, self.receiver.user.y, self.receiver.user.z]
        coord_antenna_cell = self.antenna_coord
        phase = []
        for i in range(cell_size[0]):
            for j in range(cell_size[1]):
                r = math.sqrt(
                    pow(coord_ue[0] - coord_cell[0] - coord_antenna_cell[0][j + cell_size[0] * i], 2) +
                    pow(coord_ue[1] - coord_cell[1] - coord_antenna_cell[1][j + cell_size[0] * i], 2) +
                    pow(coord_ue[2] - coord_cell[2] - coord_antenna_cell[2][j + cell_size[0] * i], 2)
                )
                cur_phase = 2 * math.pi / lambda_ * r
                phase.append(cmath.exp(complex(0, cur_phase)))
        rb_signal = RbSignal(rb_count)
        for i in range(rb_count):
            rb_signal.set_rb(i, np.array([phase]).transpose())
        return rb_signal

    def clc_propagation_loss(self)->float:
        distance_sq = (self.receiver.user.x**2 + self.receiver.user.y**2 + self.receiver.user.z**2)
        return pow(self.transmitter.lambda_/(4 * math.pi), 2) * 1/ distance_sq

    def get_antenna_coord(self):
        cell_size = self.transmitter.cell_size
        lambda_ = self.transmitter.lambda_
        antenna_x = []
        antenna_y = []
        antenna_z = []
        y = 0
        for i in range(cell_size[0]):
            x = i * lambda_ / 2 - (cell_size[0] - 1) / 4 * lambda_
            for j in range(cell_size[1]):
                z = j * lambda_ / 2 - (cell_size[1] - 1) / 4 * lambda_

                antenna_x.append(x)
                antenna_y.append(y)
                antenna_z.append(z)
        return np.array([antenna_x, antenna_y, antenna_z])
