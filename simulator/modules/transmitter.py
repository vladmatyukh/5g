from simulator.modules.module import Module
from simulator.signals import RbSignalsHelper, TxSignal
from simulator.channel_data import ChannelData
from simulator.schedule_container import ScheduleContainer


class Transmitter(Module):
    def __init__(self,
                 channel_data: ChannelData,
                 schedule_container: ScheduleContainer,
                 lambda_, cell_size, antenna):
        self.antenna = antenna
        self.cell_size = cell_size
        self.lambda_ = lambda_
        self.channel_data = channel_data
        self.schedule_container = schedule_container
        self.channels = []

    def append_channel(self, channel):
        self.channels.append(channel)

    def send_signal(self, time, signal):
        for channel in self.channels:
            channel.process(time, signal)

    def execute(self, time):
        schedule = self.schedule_container.get_schedule(time)
        if schedule is None:
            return
        signal = TxSignal()
        signal.signal = RbSignalsHelper.multiply(schedule.precoding_matrix, schedule.power_signal)
        signal.schedule = schedule
        self.send_signal(time, signal)
