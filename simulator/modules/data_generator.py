import random

from simulator.config.simulator_user import SimulatorUser
from simulator.modules.module import Module


class DataGenerator(Module):
    def __init__(self, users, buffer):
        self.buffer = buffer
        self.users = users

    def execute(self, time):
        for user in self.users:
            self.generate(time, user)

    def generate(self, time, user: SimulatorUser):
        if time < user.start_time:
            self.buffer.append_user_data(time, user.user_id, 0)
        elif time > user.stop_time:
            self.buffer.append_user_data(time, user.user_id, 0)
        else:
            data_amt = random.randint(user.size_from, user.size_to)
            self.buffer.append_user_data(time, user.user_id, data_amt)
