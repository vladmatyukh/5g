from simulator.Signals import TxSignal, RbSignal
from simulator.channel_data import ChannelData
from simulator.precoding.mrt_precoding import MrtPrecoding
from simulator.schedule import Schedule, RbSchedule


def simple_test():
    prec = MrtPrecoding()
    tx_signal = TxSignal()
    rb_amount = 1
    schedule = Schedule(rb_amount)
    schedule.set_user_antenna_amount(1, 1)
    schedule.set_user_antenna_amount(2, 1)
    rb_schedule = RbSchedule(0)
    rb_schedule.append_user(1)
    rb_schedule.append_user(2)

    channel_data = ChannelData()
    channel_matrix_1 = RbSignal(1)
    channel_matrix_1.set_rb_signal()
    pass


simple_test()