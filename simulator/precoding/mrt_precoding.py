from abc import ABC, abstractmethod

import numpy as np

from simulator.channel_data import ChannelData
from simulator.schedule import Schedule, RbSchedule
from simulator.signals import RbMatrix, RbSignalsHelper


class Precoder(ABC):
    @abstractmethod
    def build_precoding(self, precoding_matrix: RbMatrix, precoding_user_map,
                        schedule: Schedule, channel_data: ChannelData):
        pass

    @abstractmethod
    def build_rb_precoding(
            self, rb_info: RbSchedule, channel_data: ChannelData, user_antenna_amount_dict)->{}:
        """
        :param rb_info: расписание на блоке
        :param channel_data: данные канала
        :param user_antenna_amount_dict: словарь user_id - число используемых антенн
        :returns словарь user_id - вектор прекодинга
        """
        pass


class MrtPrecoder(Precoder):
    def __init__(self):
        pass

    def build_precoding(self, precoding_matrix: RbMatrix, precoding_user_map,
                        schedule: Schedule, channel_data: ChannelData):
        for rb_index, rb_info in schedule.rb_schedules.items():
            matrix = None
            precoding_user_map[rb_index] = {}
            for user in rb_info.users:
                user_channel_matrix = channel_data.get_channel_matrix(user)
                user_channel_amt = schedule.user_antenna_amount.get(user, 1)
                user_rb_matrix = user_channel_matrix.get_rb(rb_index)
                user_rb_matrix_reduced = RbSignalsHelper.extract_columns(user_rb_matrix, user_channel_amt)
                user_matrix_candidate = np.conjugate(user_rb_matrix_reduced)
                normalized = np.linalg.norm(user_matrix_candidate, axis=0)
                user_matrix = user_matrix_candidate / normalized
                if matrix is None:
                    matrix = user_matrix
                else:
                    new_matrix = np.append(matrix, user_matrix, axis=1)
                    matrix = new_matrix
                precoding_user_map[rb_index][user] = user_matrix
            precoding_matrix.set_rb(rb_index, matrix)

    def build_rb_precoding(self,
                           rb_info: RbSchedule, channel_data: ChannelData, user_antenna_amount_dict):
        precoding_map = {}
        rb_index = rb_info.rb_index
        for user in rb_info.users:
            user_channel_matrix = channel_data.get_channel_matrix(user)
            user_channel_amt = user_antenna_amount_dict.get(user, 1)
            user_rb_matrix = user_channel_matrix.get_rb(rb_index)
            user_rb_matrix_reduced = RbSignalsHelper.extract_columns(user_rb_matrix, user_channel_amt)
            user_matrix = np.conjugate(user_rb_matrix_reduced)
            normalized = np.linalg.norm(user_matrix, axis=0)
            precoding_map[user] = user_matrix / normalized
        return precoding_map
