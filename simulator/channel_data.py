class ChannelData:
    def __init__(self):
        self.channel_matrix = {}
        self.sinr = {}
        self.rate = {}
        self.user_ids = []

    def get_channel_matrix(self, user_id):
        return self.channel_matrix[user_id]

    def set_channel_matrix(self, user_id, user_channel_matrix):
        self.channel_matrix[user_id] = user_channel_matrix

