import numpy as np

from simulator.signals import RbMatrix, RbSignal


class RbSchedule:
    def __init__(self, rb_index: int):
        self.rb_index = rb_index
        self.users = []
        self.power = {}
        self.signal = None

    def user_amt(self):
        return self.users.__len__()

    def append_user(self, user_id):
        self.users.append(user_id)

    def specify_power(self, user_id, power):
        self.power[user_id] = power

    def specify_signal(self, signal: np.ndarray):
        self.signal = signal


class Schedule:

    def __init__(self, rb_count):
        self.rb_count = rb_count
        self.rb_schedules = {}
        self.user_antenna_amount = {}
        self.precoding_matrix = RbMatrix(rb_count)
        self.precoding_user_map = {}
        self.power_signal = RbSignal(rb_count)

    def set_user_antenna_amount(self, user_id, antenna_amt):
        self.user_antenna_amount[user_id] = antenna_amt

    def set_rb_schedule(self, rb_schedule: RbSchedule):
        index = rb_schedule.rb_index
        self.rb_schedules[index] = rb_schedule

    def get_rb_scheduler(self, index):
        return self.rb_schedules.get(index, None)

    def rb_amount(self):
        return self.rb_count


class ScheduleHelper:
    @staticmethod
    def empty():
        return None

    @staticmethod
    def is_empty(schedule: Schedule):
        return schedule is not None
