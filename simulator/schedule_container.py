from simulator.schedule import Schedule


class ScheduleContainer:
    def __init__(self):
        self.time_schedules = {}

    def set_schedule(self, time, value: Schedule):
        self.time_schedules[time] = value

    def get_schedule(self, time)->Schedule:
        return self.time_schedules.get(time, None)
