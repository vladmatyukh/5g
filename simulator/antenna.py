import math

import numpy as np


class Antenna:
    def __init__(self, coordinates, tilt=0, theta_scale=65, phi_scale=65, a_max=30, sla_v=30):
        self.coordinates = np.array(coordinates)
        self.tilt = tilt
        self.sla_v = sla_v
        self.a_max = a_max
        self.phi_scale = phi_scale
        self.theta_scale = theta_scale

    def three_dimensional_radiation_power_pattern(self, theta, phi):
        return - min(-(self.horizontal_cut(phi) + self.vertical_cut(theta)), self.a_max)

    def horizontal_cut(self, phi):
        return - min(12*(phi/self.phi_scale)**2, self.a_max)

    def vertical_cut(self, theta):
        return - min(12*(theta/self.theta_scale)**2, self.sla_v)

    def pattern(self, coordinates):
        delta_coordinates = coordinates - self.coordinates
        phi = math.atan(delta_coordinates[0]/delta_coordinates[1])
        theta = self.tilt + math.atan(delta_coordinates[2]/delta_coordinates[1])
        return self.three_dimensional_radiation_power_pattern(theta, phi)

    def clc_cos_beta(self, coordinates):
        y = coordinates[1] - self.coordinates[1]
        z = coordinates[2] - self.coordinates[2]
        return y/math.sqrt(y**2 + z**2)

    def clc_sin_beta(self, coordinates):
        y = coordinates[1] - self.coordinates[1]
        z = coordinates[2] - self.coordinates[2]
        return z/math.sqrt(y**2 + z**2)

    def clc_cos_alpha(self, coordinates):
        x = coordinates[0] - self.coordinates[0]
        y = coordinates[1] - self.coordinates[1]
        return y/math.sqrt(x**2 + y**2)
