from simulator.modules.module import Module


class Simulator:
    def __init__(self, step_amount: int):
        self.step_amount = step_amount
        self.modules = []

    def append_module(self, module: Module):
        self.modules.append(module)

    def run(self):
        for time in range(self.step_amount):
            print("time: {0}".format(time))
            for module in self.modules:
                module.execute(time)

