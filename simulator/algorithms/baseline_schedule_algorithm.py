import numpy as np

from simulator.algorithms.schedule_algorithm import ScheduleAlgorithm
from simulator.algorithms.utility_function import UtilityFunction
from simulator.channel_data import ChannelData
from simulator.precoding.mrt_precoding import Precoder
from simulator.schedule import Schedule, RbSchedule, ScheduleHelper


class BaselineScheduleAlgorithm(ScheduleAlgorithm):
    def __init__(self, precoder: Precoder, channel_data: ChannelData, noise, utility: UtilityFunction):
        self.noise = noise
        self.utility = utility
        self.channel_data = channel_data
        self.precoder = precoder

    def build_schedule(self, sorted_users, rb_count, cell_power, metric)->Schedule:
        if sorted_users is None:
            return ScheduleHelper.empty()
        if sorted_users.__len__() == 0:
            return ScheduleHelper.empty()
        schedule = Schedule(rb_count)
        rb_power = cell_power / rb_count
        for rb_index in range(rb_count):
            rb_schedule = RbSchedule(rb_index)
            estimate = None
            for user in sorted_users.values():
                rb_schedule.append_user(user.user_id)
                rb_schedule.specify_power(user.user_id, rb_power)
                precoding_map = self.precoder.build_rb_precoding(
                    rb_info=rb_schedule, channel_data=self.channel_data,
                    user_antenna_amount_dict={})
                next_estimate = self.utility.estimate(
                    precoding_map=precoding_map, channel_data=self.channel_data,
                    rb_schedule=rb_schedule, metric=metric, noise=self.noise, rb_index=rb_index,
                    power_denominator=rb_schedule.users.__len__())
                if estimate is None:
                    estimate = next_estimate
                elif estimate <= next_estimate:
                    estimate = next_estimate
                else:
                    rb_schedule.users.remove(user.user_id)

            amt = rb_schedule.users.__len__()
            signal = np.array([np.full(amt, rb_power/amt)]).transpose()
            rb_schedule.specify_signal(signal)
            schedule.set_rb_schedule(rb_schedule)
            schedule.power_signal.set_rb(rb_index, signal=signal)
            print("users: {0}".format(rb_schedule.users))
        for user in sorted_users.values():
            schedule.set_user_antenna_amount(user.user_id, 1)
        self.precoder.build_precoding(precoding_matrix=schedule.precoding_matrix,
                                      precoding_user_map=schedule.precoding_user_map,
                                      schedule=schedule, channel_data=self.channel_data)
        return schedule