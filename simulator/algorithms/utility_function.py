import math

import numpy as np

from simulator.channel_data import ChannelData
from simulator.helpers.sinr_helper import SinrHelper
from simulator.helpers.tb_size_helper import TbSizeHelper
from simulator.schedule import RbSchedule


class UtilityFunction:

    @staticmethod
    def estimate(precoding_map,
                 rb_index, channel_data: ChannelData, rb_schedule: RbSchedule, metric, power_denominator, noise):
        """
        :param precoding_map: прекодинг user_id - вектор прекодинга
        :param channel_data: данные пользовательских каналов
        :param rb_schedule: расписание ресурсного блока
        :param metric: метрика
        :param noise: шум
        :return: оценка эффективности расписания ресурсного блока
        """
        estimate = 0
        cell_power = np.sum(list(rb_schedule.power.items()))
        for user in rb_schedule.users:
            user_metric = metric.get(user, 1)
            channel = channel_data.get_channel_matrix(user).get_rb(rb_index=rb_index)
            u_sinr = channel_data.sinr[user]
            cqi = TbSizeHelper.cqi_from_sinr(u_sinr)
            su_sinr = TbSizeHelper.sinr_from_cqi(cqi)
            mu_sinr = SinrHelper.calculate_mu_sinr(user_id=user, users=rb_schedule.users, pw_dict=rb_schedule.power,
                                                   rb_channel=channel, rb_precoding=precoding_map,
                                                   cell_power=cell_power, su_sinr=su_sinr)
            sinr = mu_sinr
#                SinrHelper.calculate_sinr(
#                user_id=user,
#                users=rb_schedule.users,
#                pw_dict=rb_schedule.power,
#                rb_channel=channel,
#                rb_precoding=precoding_map,
#                noise=noise,
#                power_denominator=power_denominator)
            estimate = estimate + (1/user_metric) * math.log(1 + sinr)
        return estimate
