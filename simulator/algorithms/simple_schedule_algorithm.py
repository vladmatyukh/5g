import numpy as np

from simulator.algorithms.schedule_algorithm import ScheduleAlgorithm
from simulator.channel_data import ChannelData
from simulator.precoding.mrt_precoding import Precoder
from simulator.schedule import Schedule, RbSchedule, ScheduleHelper


class SimpleScheduleAlgorithm(ScheduleAlgorithm):
    def __init__(self, precoder: Precoder, channel_data: ChannelData):

        self.channel_data = channel_data
        self.precoder = precoder

    def build_schedule(self, sorted_users, rb_count, cell_power, metric)->Schedule:
        if sorted_users is None:
            return ScheduleHelper.empty()
        if sorted_users.__len__() == 0:
            return ScheduleHelper.empty()

        schedule = Schedule(rb_count)
        rb_power = cell_power/rb_count
        for rb_index in range(rb_count):
            rb_schedule = RbSchedule(rb_index)
            rb_schedule.append_user(sorted_users[rb_index % sorted_users.__len__()].user_id)
            rb_schedule.specify_power(sorted_users[rb_index % sorted_users.__len__()].user_id, rb_power)
            rb_schedule.specify_signal(np.array([rb_power]))
            schedule.set_rb_schedule(rb_schedule)
            schedule.power_signal.set_rb(rb_index, signal=np.array([rb_power]))
        for user in sorted_users.values():
            schedule.set_user_antenna_amount(user.user_id, 1)
        self.precoder.build_precoding(precoding_matrix=schedule.precoding_matrix,
                                      precoding_user_map=schedule.precoding_user_map,
                                      schedule=schedule, channel_data=self.channel_data)
        return schedule
