from abc import ABC, abstractmethod

from simulator.channel_data import ChannelData
from simulator.precoding.mrt_precoding import Precoder
from simulator.schedule import RbSchedule


class PfUtilityFunction(ABC):

    @abstractmethod
    def estimate(self, precoder: Precoder, channel_data: ChannelData, rb_schedule: RbSchedule, metric, noise):
        pass
