from abc import ABC, abstractmethod

from simulator.schedule import Schedule


class ScheduleAlgorithm(ABC):
    @abstractmethod
    def build_schedule(self, sorted_users, rb_count, cell_power, metric) -> Schedule:
        pass