import numpy as np

from simulator.algorithms.schedule_algorithm import ScheduleAlgorithm
from simulator.algorithms.utility_function import UtilityFunction
from simulator.channel_data import ChannelData
from simulator.precoding.mrt_precoding import Precoder
from simulator.schedule import Schedule, RbSchedule, ScheduleHelper


class ColoringScheduleAlgorithm(ScheduleAlgorithm):
    def __init__(self, precoder: Precoder, channel_data: ChannelData, noise, utility: UtilityFunction, boundary):
        self.noise = noise
        self.utility = utility
        self.channel_data = channel_data
        self.precoder = precoder
        self.boundary = boundary

    def build_channel_matrix(self, sorted_users, rb_index):
        user_amt = len(sorted_users)
        matrix = np.full((user_amt, user_amt), 0.0)
        for i in range(user_amt):
            user_i = sorted_users[i].user_id
            channel_i = self.channel_data.get_channel_matrix(user_i).get_rb(rb_index)
            transposed = channel_i.transpose().conjugate() / np.linalg.norm(channel_i)
            for j in range(user_amt):
                user_j = sorted_users[j].user_id
                channel_j = self.channel_data.get_channel_matrix(user_j).get_rb(rb_index)
                a = np.matmul(transposed, channel_j / np.linalg.norm(channel_j))[0][0]
                matrix[i][j] = (abs(a).real ** 2 >= self.boundary) * (i != j)
        return matrix

    @staticmethod
    def build_color_array(matrix, user_amt):
        users_array = list(range(user_amt))
        coloring_arrays = []
        scheduled = []
        for i in users_array:
            if scheduled.count(i) > 0:
                continue
            row = matrix[i]
            for j in scheduled:
                row[j] = 1
            scheduled.append(i)
            for j in range(i+1, user_amt):
                if scheduled.count(j) > 0:
                    row[j] = 1
                    continue
                if row[j] == 0:
                    row = row + matrix[j]
                    scheduled.append(j)
            coloring_arrays.append(row)
            print("row: {0}".format(row))
        return coloring_arrays

    @staticmethod
    def build_candidate_schedule(sorted_users, rb_index, coloring_array, user_amt)->RbSchedule:
        schedule = RbSchedule(rb_index)
        for i in range(user_amt):
            if coloring_array[i] == 0:
                schedule.append_user(sorted_users[i].user_id)
        return schedule

    def build_rb_schedule(self, sorted_users, rb_index, metric, rb_power):
        local_matrix = self.build_channel_matrix(sorted_users, rb_index)
        colors = self.build_color_array(matrix=local_matrix, user_amt=len(sorted_users))
        result = None
        estimate = 0
        print("start")
        for arr in colors:
            candidate = self.build_candidate_schedule(sorted_users, rb_index, arr, len(sorted_users))
            user_power = rb_power/candidate.user_amt()
            for user in candidate.users:
                candidate.specify_power(user, user_power)
            precoding_map = self.precoder.build_rb_precoding(
                rb_info=candidate, channel_data=self.channel_data,
                user_antenna_amount_dict={})

            next_estimate = self.utility.estimate(
                precoding_map=precoding_map, channel_data=self.channel_data,
                rb_schedule=candidate, metric=metric, noise=self.noise, rb_index=rb_index,
                power_denominator=1)
            print("candidate: {0}, estimate: {1}".format(candidate.users, next_estimate))
            if result is None:
                result = candidate
                estimate = next_estimate
            elif estimate < next_estimate:
                result = candidate
                estimate = next_estimate
        print("stop")
        return result

    def build_schedule(self, sorted_users, rb_count, cell_power, metric)->Schedule:
        if sorted_users is None:
            return ScheduleHelper.empty()
        if sorted_users.__len__() == 0:
            return ScheduleHelper.empty()
        schedule = Schedule(rb_count)
        rb_power = cell_power / rb_count
        for rb_index in range(rb_count):
            rb_schedule = self.build_rb_schedule(sorted_users, rb_index, metric, rb_power)
            amt = len(rb_schedule.users)
            signal = np.array([np.full(amt, rb_power/amt)]).transpose()
            rb_schedule.specify_signal(signal)
            schedule.set_rb_schedule(rb_schedule)
            schedule.power_signal.set_rb(rb_index, signal=signal)
            print("users: {0}".format(rb_schedule.users))
        for user in sorted_users.values():
            schedule.set_user_antenna_amount(user.user_id, 1)
        self.precoder.build_precoding(precoding_matrix=schedule.precoding_matrix,
                                      precoding_user_map=schedule.precoding_user_map,
                                      schedule=schedule, channel_data=self.channel_data)
        return schedule