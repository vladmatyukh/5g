import numpy as np


class DoGraph:

    def __init__(self, channel_data, buffer, users):
        self.pfu = np.ones(users)
        self.users = users
        self.buffer = buffer
        self.channel_data = channel_data

    def matrix_compilation(self, time, pfu):
        user_massive = []
        chanel_matrix = []
        for i in range(self.users):
            if self.buffer[i] > 0:
                user_massive.append(i)
        for i in range(len(user_massive)):
            chanel_matrix.append(self.channel_data.get_channel_matrix(i))
        B = np.zeros((len(user_massive), len(user_massive) + 1))
        for i in range(len(user_massive)):
            for j in range(len(user_massive)):
                if np.vdot(chanel_matrix[i] / np.linalg.norm(chanel_matrix[i]),
                           chanel_matrix[j] / np.linalg.norm(chanel_matrix[j])) > 0.3 and i != j:
                    B[i][j] = 1
                if i == j:
                    B[i][j] = 1
        for i in range(len(user_massive)):
            B[i][len(user_massive)] = user_massive[i]
        for i in range(len(user_massive) - 1):
            for j in range(len(user_massive) - i - 1):
                if self.pfu[user_massive[j]] < self.pfu[user_massive[j+1]]:
                    B[j], B[j+1] = B[j+1], B[j].copy()
                    B[:, j], B[:, j + 1] = B[:, j + 1], B[:, j].copy()
        return B




