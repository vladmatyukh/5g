import numpy as np


class TxSignal:
    def __init__(self):
        self.schedule = None
        self.signal = None


class RxSignal:
    def __init__(self):
        self.tx_signal = None
        self.channel_matrix = None
        self.signal = None


class RbSignal:
    def __init__(self, rb_count):
        self.rb_count = rb_count
        self.rbs = {}

    def set_rb(self, rb_index, signal: np.ndarray):
        self.rbs[rb_index] = signal

    def get_rb(self, rb_index)->np.ndarray:
        return self.rbs.get(rb_index, None)


class RbMatrix:
    def __init__(self, rb_count):
        self.rb_count = rb_count
        self.rbs = {}

    def set_rb(self, rb_index, matrix: np.ndarray):
        self.rbs[rb_index] = matrix

    def get_rb(self, rb_index)->np.ndarray:
        return self.rbs.get(rb_index, None)


class RbSignalsHelper:
    @staticmethod
    def multiply_matrix_number(rb_matrix: RbMatrix, number):
        matrix = RbMatrix(rb_matrix.rb_count)
        for i in range(rb_matrix.rb_count):
            rb_value = rb_matrix.get_rb(i)
            if rb_value is None:
                continue
            new_matrix = rb_value * number
            matrix.set_rb(i, new_matrix)
        return matrix

    @staticmethod
    def multiply_signal_number(rb_signal: RbSignal, number):
        signal = RbSignal(rb_signal.rb_count)
        for i in range(rb_signal.rb_count):
            rb_value = rb_signal.get_rb(i)
            if rb_value is None:
                continue
            val = rb_value * number
            signal.set_rb(i, val)
        return signal

    @staticmethod
    def extract_rb_columns(rb_matrix: RbMatrix, col_amt:int):
        matrix = RbMatrix(rb_matrix.rb_count)
        for i in range(rb_matrix.rb_count):
            rb_value = rb_matrix.get_rb(i)
            if rb_value is None:
                continue
            new_matrix = RbSignalsHelper.extract_columns(rb_value, col_amt)
            matrix.set_rb(i, new_matrix)
        return matrix

    @staticmethod
    def extract_columns(matrix, col_amt:int):
        idx = np.array(range(col_amt))
        new_matrix = matrix[:, idx]
        return new_matrix

    @staticmethod
    def multiply(rb_matrix: RbMatrix, rb_signal: RbSignal):
        new_signal = RbSignal(rb_signal.rb_count)
        for i in range(rb_signal.rb_count):
            if rb_signal.rbs.get(i, None) is None:
                continue
            if rb_matrix.rbs.get(i, None) is None:
                continue
            rb_matrix_item = rb_matrix.rbs[i]
            rb_signal_item = rb_signal.rbs[i]
            signal = np.matmul(rb_matrix_item, rb_signal_item)
            new_signal.set_rb(i, signal)
        return new_signal

    @staticmethod
    def multiply_component(rb_signal_left: RbSignal, rb_signal_right: RbSignal):
        new_signal = RbSignal(rb_signal_left.rb_count)
        for i in range(new_signal.rb_count):
            if rb_signal_left.rbs[i] is None:
                continue
            if rb_signal_right.rbs[i] is None:
                continue
            signal = np.matmul(rb_signal_left.rbs[i].transpose(), rb_signal_right.rbs[i])
            new_signal.set_rb(i, signal)
        return new_signal
