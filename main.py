import time

from scenarios.sample_simulator_scenario import SampleSimulatorScenario
from scenarios.uniform_simulator_scenario import UniformSimulatorScenario
from simulator.algorithms.baseline_schedule_algorithm import BaselineScheduleAlgorithm
from simulator.algorithms.coloring_schedule_algorithm import ColoringScheduleAlgorithm
from simulator.algorithms.simple_schedule_algorithm import SimpleScheduleAlgorithm
from simulator.algorithms.utility_function import UtilityFunction
from simulator.buffer import Buffer
from simulator.channel_data import ChannelData
from simulator.config.simulator_config import SimulatorConfig
from simulator.config.simulator_user import SimulatorUser
from simulator.helpers.json_helper import JsonHelper
from simulator.helpers.sinr_helper import SinrHelper
from simulator.modules.basic_scheduler import BasicScheduler
from simulator.modules.channel import Channel
from simulator.modules.data_generator import DataGenerator
from simulator.modules.receiver import Receiver
from simulator.modules.transmitter import Transmitter
from simulator.precoding.mrt_precoding import MrtPrecoder
from simulator.schedule_container import ScheduleContainer
from simulator.simulator import Simulator


import numpy as np


def init_simulator(config: SimulatorConfig, buffer: Buffer)->Simulator:
    simulator = Simulator(config.stepAmount)
    generator = DataGenerator(config.users, buffer)
    simulator.append_module(generator)
    schedule_container = ScheduleContainer()
    channel_data = ChannelData()
    #do_graph = DoGraph(channel_data, buffer, config.users)
    precoder = MrtPrecoder()
    #algo = BaselineScheduleAlgorithm(precoder=precoder, channel_data=channel_data, noise=config.noise, utility=UtilityFunction())
    algo = ColoringScheduleAlgorithm(precoder=precoder, channel_data=channel_data, noise=config.noise, utility=UtilityFunction(), boundary=0.2)
    scheduler = BasicScheduler(schedule_container=schedule_container, buffer=buffer, channel_data=channel_data,
                               users=[user.user_id for user in config.users],rb_count=config.ndlrb, delta=config.delta,
                               precoder=precoder, noise=config.noise, power=config.power
                               , schedule_algorithm=algo)
    simulator.append_module(scheduler)
    transmitter = Transmitter(
        channel_data=channel_data,
        schedule_container=schedule_container,
        lambda_=config.lambda_,
        cell_size=config.cell_size,
        antenna=config.antenna
    )
    for user in config.users:
        receiver = Receiver(config.noise, user, buffer, channel_data)
        channel = Channel(transmitter, receiver)
        transmitter.append_channel(channel)
        channel_matrix = channel.clc_channel_matrix(config.ndlrb)
        channel_data.set_channel_matrix(user.user_id, channel_matrix)
        channel_data.sinr[user.user_id] = SinrHelper.calculate_su_sinr(channel_matrix, config.noise)

    user_amt = len(config.users)
    matr = np.full((user_amt,user_amt), 0.0)
    for i in range(user_amt):
        user_i = config.users[i].user_id
        channel_i = channel_data.get_channel_matrix(user_i).get_rb(0)
        transposed = channel_i.transpose().conjugate()/np.linalg.norm(channel_i)
        for j in range(user_amt):
            user_j = config.users[j].user_id
            channel_j = channel_data.get_channel_matrix(user_j).get_rb(0)
            a = np.matmul(transposed, channel_j/np.linalg.norm(channel_j))[0][0]
            matr[i][j] = round(abs(a).real**2, 2)
            if matr[i][j] == 0:
                print("AAA: {0}; {1}".format(a, abs(a).real**2))

    print(matr)

    print(abs(np.matmul(channel_data.channel_matrix[1].rbs[0].transpose().conjugate(), channel_data.channel_matrix[2].rbs[0])[0][0]/
          (np.linalg.norm(channel_data.channel_matrix[1].rbs[0])*np.linalg.norm(channel_data.channel_matrix[2].rbs[0]))))

    simulator.append_module(transmitter)
    return simulator


def run(config_file):
    scenario = UniformSimulatorScenario(user_amount=15)
    config = scenario.load_config()
    buffer = Buffer()
    simulator = init_simulator(config, buffer)

    simulator.run()
    gg = time.time()
    JsonHelper.save("{0}_transferred_{1}.json".format(scenario.name(), gg), buffer.data_transferred)
    JsonHelper.save("{0}_buffer_value_{1}.json".format(scenario.name(), gg), buffer.buffer_value)
    JsonHelper.save("{0}_blocked_data_{1}.json".format(scenario.name(), gg), buffer.blocked_data)


run("myconfig.json")
